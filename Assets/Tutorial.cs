﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class Tutorial : MonoBehaviour
{
    [SerializeField] Transform finger;
    [SerializeField] Transform touch;
    [SerializeField] Transform ship;
    [SerializeField] Transform description;
    [SerializeField] List<Sprite> listMove;
    [SerializeField] Transform image;
    [SerializeField] List<Button> listButton;
    static bool oneTimeTutorial = true;
    float angle = 270f;
    float timeToChange = 1f;
    int counter = 0;
    float delta;
    int moveChange = 0;
    // Start is called before the first frame update
    void Start()
    {
        if(!oneTimeTutorial)
        {
            gameObject.SetActive(false);
        }
        listButton[0].GetComponent<Button>().onClick.AddListener(Change);
        listButton[1].GetComponent<Button>().onClick.AddListener(Back);
        oneTimeTutorial = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(moveChange == 0)
        {
            Move1();
            transform.Find("Title").GetComponentInChildren<Text>().text = "Move 1";
            description.GetComponent<Text>().text = "You can move freely";
            image.GetComponent<Image>().sprite = listMove[0];
            listButton[1].gameObject.SetActive(false);
        }
        else if(moveChange == 1)
        {
            Move2();
            transform.Find("Title").GetComponentInChildren<Text>().text = "Move 2";
            description.GetComponent<Text>().text = "The ship will move on your fingertips";
            image.GetComponent<Image>().sprite = listMove[1];
            listButton[1].gameObject.SetActive(true);
        }
        else
        {
            Time.timeScale = 1;
            this.gameObject.SetActive(false);
        }
    }
    public void Change()
    {
        counter = 0;
        moveChange ++;
        timeToChange = 1f;
        angle = 270f;
    }
    public void Back()
    {
        counter = 0;
        moveChange --;
        timeToChange = 1f;
        angle = 270f;
    }
    void Move1()
    {
        switch(counter)
        {
            case 0:
            {
                if(timeToChange <= 0)
                {
                    counter++;
                }
                else
                {
                    timeToChange -= Time.deltaTime;
                }
                finger.gameObject.SetActive(true);
                touch.gameObject.SetActive(false);
                finger.position = new Vector3(540f, 640f, 0f);
                ship.position = new Vector3(540f, 860f, 0f);
                break;
            }
            case 1:
            {
                finger.gameObject.SetActive(false);
                touch.gameObject.SetActive(true);
                float newX = 540f + Mathf.Cos(angle*Mathf.Deg2Rad)*80f;
                float newY = 720f + Mathf.Sin(angle*Mathf.Deg2Rad)*80f;
                angle = angle + 180f*Time.deltaTime;
                touch.position = new Vector3(newX, newY, 0f);
                ship.position = touch.position + new Vector3(0f, 220f, 0f);
                if(angle >= 630)
                {
                    timeToChange = 1f;
                    counter = 0;
                    angle = 270f;
                }
                break;
            }
        }
    }
    void Move2()
    {
        switch(counter)
        {
            case 0:
            {
                if(timeToChange <= 0)
                {
                    counter++;
                }
                else
                {
                    timeToChange -= Time.deltaTime;
                }
                finger.gameObject.SetActive(true);
                touch.gameObject.SetActive(false);
                finger.position = new Vector3(540f, 640f, 0f);
                ship.position = new Vector3(540f, 900f, 0f);
                break;
            }
            case 1:
            {
                finger.gameObject.SetActive(false);
                touch.gameObject.SetActive(true);
                touch.position = finger.position;
                if(ship.position.y > 730f)
                {
                    ship.position -= new Vector3(0f, 100f*Time.deltaTime, 0f);
                }
                else
                {
                    counter++;
                    delta = ship.position.y - touch.position.y;
                }
                break;
            }
            case 2:
            {
                float newX = 540f + Mathf.Cos(angle*Mathf.Deg2Rad)*80f;
                float newY = 720f + Mathf.Sin(angle*Mathf.Deg2Rad)*80f;
                angle = angle + 180f*Time.deltaTime;
                touch.position = new Vector3(newX, newY, 0f);
                ship.position = touch.position + new Vector3(0f, delta, 0f);
                if(angle >= 630)
                {
                    timeToChange = 1f;
                    counter = 0;
                    angle = 270f;
                }
                break;
            }
        }
    }
}
