﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Equipment", menuName = "Inventory/Equipment")]

public class Equipment : Item
{
    public EquipSlot equipSlot;
    public bool isEquiped = false;
    public bool isPossess = false;
    public string description = " ";

    public int upgradeMaterial = 100;
    public int Level = 1;
    public float armorModifier;
    public float damageModifier;
    public float hpModifier;


    public override void Use()
    {
        base.Use();
        
        UpgradeUI.Instance.setActive(true, this);
    }
    public void Upgrade()
    {
        Level++;
        upgradeMaterial += (50*Level);
        armorModifier = Mathf.Ceil(1.2f*armorModifier);
        damageModifier = Mathf.Ceil(1.2f*damageModifier);
        hpModifier = Mathf.Ceil(1.2f*hpModifier);
    }
}

public enum EquipSlot {Weapon, Shield, Mini_Ship, Core};