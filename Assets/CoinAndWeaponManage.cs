﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAndWeaponManage : MonoBehaviour
{
    public User user{get;set;}
    public int Coins{get;set;}

    [SerializeField] List<Equipment> listWeaponOfGame;
    public List<Equipment> listWeaponOfPlayer;
    
    void Start()
    {
        user = new User();
        UserData data = SaveSystem.LoadUser();

        if(data != null)
        {
            Coins = data.coins;

            user.coins = data.coins;
            user.currentLevel = data.currentLevel;
        }
        else
        {
            Coins = 10000;

            user.coins = 10000;
            user.currentLevel = 1;

            user.SaveUserState();
        }

        foreach(Equipment x in listWeaponOfGame)
        {
            if(x.isPossess)
            {
                listWeaponOfPlayer.Add(x);
            }
        }
    }
}
