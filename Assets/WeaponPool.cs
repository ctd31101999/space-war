﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WeaponPool : MonoBehaviour
{
    public Equipment[] listItem;
    public Transform[] listSlot;
    [SerializeField] GameObject buyPanel;
    void Start()
    {
        for(int i = 0 ;i < listSlot.Length ; i++)
        {
            listSlot[i].GetComponent<ShopItemBtn>().buyPanel = buyPanel;
            listSlot[i].GetComponent<ShopItemBtn>().ItemBuy = listItem[i];
            listSlot[i].GetChild(0).GetComponent<Text>().text = listItem[i].name;
            listSlot[i].GetChild(1).GetComponent<Image>().sprite = listItem[i].icon;
            listSlot[i].GetChild(2).GetComponentInChildren<Text>().text = listItem[i].upgradeMaterial.ToString();
        }
    }
    
    void Update()
    {
        for(int i = 0 ;i < listSlot.Length ; i++)
        {
            if(listItem[i].isPossess)
            {
                listSlot[i].GetChild(1).GetComponent<Image>().color = new Color(255f, 255f, 255f, 1f);
                listSlot[i].GetChild(3).gameObject.SetActive(true);
                listSlot[i].GetComponent<Button>().enabled = false;
            } 
        }
    }
}
