﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public float damage;
    public float fireRate;
    public float bulletSpeed;
    public GameObject bulletPrefab;
    public abstract void Shoot();
}
