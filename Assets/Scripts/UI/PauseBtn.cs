﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseBtn : MonoBehaviour
{
    public GameObject pausePanel;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(Pause);
    }
    
    private void Pause()
    {
        pausePanel.SetActive(true);
        Time.timeScale = 0;
    }
}
