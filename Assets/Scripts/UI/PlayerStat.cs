﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStat : MonoBehaviour
{
    #region Singleton
    public static PlayerStat Instance;

    void Awake()
    {
        Instance = this;
        playerAttack = 0;
        playerHP = 0;

        UpdateUI();
        EquipmentManager.Instance.onEquipmentChanged += UpdateStat;
        UpgradeUI.Instance.onEquipmentUpgrade += UpdateStat;
    }
    #endregion
    public Text playerAttackText;
    public Text playerHPText;
    public float playerAttack;
    public float  playerHP;
    
    void UpdateStat(Equipment newEquip, Equipment oldEquip)
    {
        playerAttack = 0;
        playerHP = 0;
        for(int i = 0; i < EquipmentManager.Instance.currentEquipment.Length; i++)
        {
            if(EquipmentManager.Instance.currentEquipment[i] != null)
            {
                Debug.Log(EquipmentManager.Instance.currentEquipment[i].name + ": " + "attack = " + EquipmentManager.Instance.currentEquipment[i].damageModifier+ " - " + "HP = " + EquipmentManager.Instance.currentEquipment[i].hpModifier);
                playerAttack += EquipmentManager.Instance.currentEquipment[i].damageModifier;
                playerHP += EquipmentManager.Instance.currentEquipment[i].hpModifier;
            }          
        }

        UpdateUI();
    }

    public void FirstLoadforEquipmentSlot(Equipment equipment)
    {
        playerAttack += equipment.damageModifier;
        playerHP += equipment.hpModifier;

        UpdateUI();
    }

    void UpdateUI()
    {
        playerAttackText.text = "Attack " + playerAttack;
        playerHPText.text = "HP " + playerHP;
    }
}
