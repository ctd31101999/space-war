﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemBtn : MonoBehaviour
{
    public GameObject buyPanel;
    public Item ItemBuy;

    Text InformText;
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(Click);
    }

    private void Click()
    {
        buyPanel.transform.Find("Image").Find("Name").GetComponent<Text>().text = transform.Find("Name").GetComponent<Text>().text;
        buyPanel.transform.Find("Image").Find("Image").Find("Item").GetComponent<Image>().sprite = transform.Find("Image").GetComponent<Image>().sprite;
        buyPanel.transform.Find("Image").Find("Buy Button").Find("Price").Find("Price").GetComponent<Text>().text = transform.Find("Price").Find("Price").GetComponent<Text>().text;
        buyPanel.transform.Find("Image").Find("Buy Button").Find("Price").Find("Currency").GetComponent<Image>().sprite = transform.Find("Price").Find("Currency").GetComponent<Image>().sprite;
        buyPanel.transform.Find("Image").Find("Inform Text").GetComponent<Text>().gameObject.SetActive(false);
        buyPanel.GetComponentInChildren<BuyBtn>().itemBuy = ItemBuy;
        buyPanel.SetActive(true);
    }
}
