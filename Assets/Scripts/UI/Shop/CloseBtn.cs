﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloseBtn : MonoBehaviour
{
    [SerializeField] GameObject buyPanel;
    
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(Close);
    }

    private void Close()
    {
        buyPanel.SetActive(false);
    }
}
