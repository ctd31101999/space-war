﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyBtn : MonoBehaviour
{
    public Item itemBuy;
    [SerializeField] Transform storage;
    public Text informText;
    void Start()
    {   
        if(GameObject.Find("Game Controller").GetComponent<CoinAndWeaponManage>().Coins < int.Parse( GetComponentInChildren<Text>().text ))
        {
            GetComponent<Button>().enabled = false;
        }
        GetComponent<Button>().onClick.AddListener(Buy);
    }

    private void Buy()
    {
        if(Purchase())
        {
            transform.parent.parent.gameObject.SetActive(false);
        }
    }

    private bool Purchase()
    {
        Equipment temp = (Equipment)itemBuy;
        if(GameObject.Find("Game Controller").GetComponent<CoinAndWeaponManage>().Coins < int.Parse( GetComponentInChildren<Text>().text ))
        {
            informText.gameObject.SetActive(true);
            return false;
        }else
        {
            GameObject.Find("Game Controller").GetComponent<CoinAndWeaponManage>().Coins -= int.Parse( GetComponentInChildren<Text>().text );
            GameObject.Find("Game Controller").GetComponent<CoinAndWeaponManage>().user.coins -= int.Parse( GetComponentInChildren<Text>().text );
            GameObject.Find("Game Controller").GetComponent<CoinAndWeaponManage>().user.SaveUserState();

            GameObject.Find("Game Controller").GetComponent<CoinAndWeaponManage>().listWeaponOfPlayer.Add(temp);
            storage.GetComponent<InventoryUI>().oneTime = true;
            temp.isPossess = true;
        }       
        return true;
    }
}
