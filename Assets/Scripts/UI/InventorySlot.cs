﻿using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public Button removeButton;
    public Image icon;
    public Item item;
    bool oneTime = true;
    void Awake()
    {
        if(item != null)
        {
            //Inventory.Instance.Add(item);
            icon.sprite = item.icon;
            icon.enabled = true;
            Debug.Log(this.transform.name + " icon: " + icon.enabled);
            removeButton.interactable = true;
            oneTime = false;
        }
        else
        {
            icon.enabled = false;
            removeButton.interactable = false;
        }
    }

    void Update()
    {
        if(oneTime && item != null)
        {
            icon.sprite = item.icon;
            icon.enabled = true;
            removeButton.interactable = true;
            oneTime = false;
        }
    }

    public void AddItime(Item newItem)
    {
        item = newItem;
        icon.sprite = item.icon;
        icon.enabled = true;
        removeButton.interactable = true;
    }

    public void ClearSlot()
    {
        item = null;
        icon.sprite = null;
        icon.enabled = false;
        removeButton.interactable = false;
    }

    public void OnRemoveButton()
    {
        Inventory.Instance.Remove(item);
    }

    public void UseItem()
    {
        if(item != null)
        {
            item.Use();
            Debug.Log("Test");
        }
        
    }
}
