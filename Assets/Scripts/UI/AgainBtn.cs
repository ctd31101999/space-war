﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AgainBtn : MonoBehaviour
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(Again);
    }
    
    private void Again()
    {
        SceneManager.LoadScene("Main");
    }
}
