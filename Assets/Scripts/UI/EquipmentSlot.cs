﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentSlot : MonoBehaviour
{
    public Button removeButton;
    public Image icon;
    public Equipment equipment;
    public EquipSlot slotIndex;
    public EquipmentSlot slotFillFirst;
    void Awake()
    {
        EquipmentManager.Instance.onEquipmentChanged+=UpdateUI;

        if(equipment != null)
        {
            EquipmentManager.Instance.Equip(equipment);
            icon.sprite = equipment.icon;
            icon.enabled = true;
            Debug.Log(this.transform.name + " icon: " + icon.enabled);
            removeButton.interactable = true;

            PlayerStat.Instance.FirstLoadforEquipmentSlot(equipment);
        }
        else
        {
            icon.enabled = false;
            removeButton.interactable = false;
        }
    }

    public void AddEquipment(Equipment newEquip)
    {
        equipment = newEquip;
        icon.sprite = equipment.icon;
        icon.enabled = true;
        removeButton.interactable = true;
    }

    public void UnEquip()
    {
        if(equipment != null) EquipmentManager.Instance.UnEquip((int)slotIndex);
        equipment = null;
        icon.sprite = null;
        icon.enabled = false;
        removeButton.interactable = false;
    }

    public bool isEmpty()
    {
        if(equipment != null) return false;
        return true;
    }

    void UpdateUI(Equipment newEquip, Equipment oldEquip)
    {
        if(newEquip != null)
        {
            if(newEquip.equipSlot == this.slotIndex)
                {
                    if(slotFillFirst != null)
                        if(slotFillFirst.isEmpty()) return;
                    AddEquipment(newEquip);
                }
        }else
        {
            if(oldEquip.equipSlot == this.slotIndex)
                UnEquip();
        }
    }

    public void UseItem()
    {
        if(equipment != null)
            UpgradeUI.Instance.setActive(true, equipment);
    }
}
