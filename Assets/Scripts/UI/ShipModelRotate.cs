﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipModelRotate : MonoBehaviour
{
    
    void Update()
    {
        transform.RotateAround(transform.position, new Vector3(0f,1f,0f), Time.deltaTime * 40f);
    }
}
