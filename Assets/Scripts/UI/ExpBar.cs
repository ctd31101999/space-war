﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpBar : MonoBehaviour
{
    public int level;
    public int experience;
    public int experienceNeedToLevelUp;
    public Slider levelUpBar;
    public Text currentLevel;

    public static GameObject gameController;
    public static UIManager uIManager;
    void Start()
    {
        gameController = GameObject.Find("Main Camera");
        uIManager = gameController.GetComponent<UIManager>();
        level = 1;
        experience = 0;
        experienceNeedToLevelUp = 10;
        levelUpBar.value = experience;
        levelUpBar.maxValue = experienceNeedToLevelUp;
        currentLevel.text = "1";
    }
    public void IncreaseExp(int exp)
    {
        experience += exp;
        if(experience >= experienceNeedToLevelUp)
        {
            experience -= experienceNeedToLevelUp;
            IncreaseLevel();
        }

        levelUpBar.value = experience;
    }
    public void IncreaseLevel()
    {
        uIManager.update();
        levelUpBar.value = experience;

        experienceNeedToLevelUp += 10;
        levelUpBar.maxValue = experienceNeedToLevelUp;

        level += 1;
        currentLevel.text = level.ToString();
    }
}
