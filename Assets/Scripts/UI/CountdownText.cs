﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownText : MonoBehaviour
{
    public int countdownTime;
    public GameObject pauseMenu;
    public GameObject pausePanel;

    private int currentTime;

    void OnEnable()
    {
        currentTime = countdownTime;
        StartCoroutine(CountdownTimer());
    }

    IEnumerator CountdownTimer()
    {   
        while (currentTime > 0)
        {
            GetComponent<Text>().text = currentTime.ToString();
            yield return new WaitForSeconds(.0001f);
            currentTime--;
        }
        GetComponent<Text>().text = "GO!";
        yield return new WaitForSeconds(.0001f);
        Time.timeScale = 1;
        GetComponent<Text>().gameObject.SetActive(false);
        
        pauseMenu.SetActive(true);
        pausePanel.SetActive(false);
    }
}
