﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    public Transform itemsParent;
    Inventory inventory;
    InventorySlot[] slots;
    public delegate void StorageWeapon();
    static public StorageWeapon storageWeapon ;
    public bool oneTime = true;
    void Start()
    {
        storageWeapon = UpdateWeapon;
        inventory = Inventory.Instance;
        inventory.onItemChangedCallback += UpdateUI;
        slots = itemsParent.GetComponentsInChildren<InventorySlot>();
        storageWeapon();
    }

    void UpdateUI()
    {
        for(int i = 0; i < slots.Length; i++)
        {
            if(i < inventory.items.Count)
            {
                slots[i].AddItime(inventory.items[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }
        }
    }

    public void UpdateWeapon()
    {
        if(oneTime)
        {
            Inventory.Instance.items.Clear();
            foreach(Equipment x in GameObject.Find("Game Controller").GetComponent<CoinAndWeaponManage>().listWeaponOfPlayer)
            {
                Inventory.Instance.Add(x);    
            }
            for(int i = 0; i < EquipmentManager.Instance.currentEquipment.Length ; i++ )
            {
                EquipmentManager.Instance.currentEquipment[i] = null;
            }
            for(int i = 0 ; i < 20 ; i++)
            {
                Equipment temp = (Equipment)itemsParent.GetChild(i).GetComponent<InventorySlot>().item;
                if(temp != null && temp.isEquiped)
                {
                    EquipmentManager.Instance.Equip(temp);
                    temp.RemoveFromInventory();
                }   
            }
            oneTime = false;
        }
    }
}
