﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateMenu : MonoBehaviour
{
    public static GameObject gameController;
    public static UIManager uIManager;
    public static GameManager gameManager;
    public int updatePoint;
    public Button upgrade1Button;
    public Button upgrade2Button;
    public Button upgrade3Button;
    [SerializeField] List<Sprite> listSkillImage;
    List<Button> buttons = new List<Button>();
    public static Player player;
 
    void Start()
    {
        gameController = GameObject.Find("Game Controller");
        uIManager = gameController.GetComponentInChildren<UIManager>();
        gameManager = gameController.GetComponentInChildren<GameManager>();
        buttons.Add(upgrade1Button);
        buttons.Add(upgrade2Button);
        buttons.Add(upgrade3Button);
        player = GameObject.Find("Player").GetComponent<Player>();
        SetUpdateMenu(); 
    }

    void OnEnable()
    {
       SetUpdateMenu();
    }

    void SetUpdateMenu()
    {
        int rand;
        List<int> have = new List<int>();
        string[] temp = transform.GetChild(0).GetComponent<Text>().text.Split(' ');
        temp[2] = updatePoint.ToString();
        temp[3] = (updatePoint > 1)?"POINTS":"POINT";
        transform.GetChild(0).GetComponent<Text>().text = string.Join(" ",temp);
        foreach (Button button in buttons)
        {
            button.onClick.RemoveAllListeners();
            do
            {
                rand = Random.Range(1,6);
            }while(have.Contains(rand));
            have.Add(rand);
            switch (rand)
            {
                case 1:
                    button.onClick.AddListener(IncreaseBullet);
                    button.GetComponentInChildren<Text>().text = "Bullet";
                    break;
                case 2:
                    button.onClick.AddListener(IncreaseDamage);
                    button.GetComponentInChildren<Text>().text = "Damage";
                    break;
                case 3: 
                    button.onClick.AddListener(RestoreHP);
                    button.GetComponentInChildren<Text>().text = "Restore HP";
                    break;
                case 4:
                    button.onClick.AddListener(IncreaseHP);
                    button.GetComponentInChildren<Text>().text = "Increase HP";
                    break;
                case 5:
                    button.onClick.AddListener(DrainHP);
                    button.GetComponentInChildren<Text>().text = "Drain HP";
                    break;
                case 6:
                    button.onClick.AddListener(SlowEnemyBulletSpeed);
                    button.GetComponentInChildren<Text>().text = "Slow Bullet Speed";
                    break;
                default:
                    break;
            }
            button.GetComponent<Image>().sprite = listSkillImage[rand-1];
        }
        Time.timeScale = 0;
    }

    public void IncreaseBullet()
    {
        Time.timeScale = 1;
        if( GameObject.Find("Player").transform.GetChild(0).gameObject.activeInHierarchy )
        {
            GameObject.Find("Player").GetComponentInChildren<NormalGun>().numberBullet += 2;
        }
        if( GameObject.Find("Player").transform.GetChild(1).gameObject.activeInHierarchy )
        {
            GameObject.Find("Player").GetComponentInChildren<SonicGun>().numberBullet += 2;
        }
        if( GameObject.Find("Player").transform.GetChild(2).gameObject.activeInHierarchy )
        {
            GameObject.Find("Player").GetComponentInChildren<LaserGun>().Width += 1f;
            GameObject.Find("Player").GetComponentInChildren<LaserGun>().damage += 0.5f;
            GameObject.Find("Player").GetComponentInChildren<LaserGun>().oneTime = true;
        }
        updatePoint--;
        if(updatePoint <=0 ) gameObject.SetActive(false);
        else SetUpdateMenu();
    }

    public void IncreaseDamage()
    {
        Time.timeScale = 1;
        if( GameObject.Find("Player").transform.GetChild(0).gameObject.activeInHierarchy )
        {
            GameObject.Find("Player").GetComponentInChildren<NormalGun>().damage += 20;
        }
        if( GameObject.Find("Player").transform.GetChild(1).gameObject.activeInHierarchy )
        {
            GameObject.Find("Player").GetComponentInChildren<SonicGun>().damage += 10;
        }
        if( GameObject.Find("Player").transform.GetChild(2).gameObject.activeInHierarchy )
        {
            GameObject.Find("Player").GetComponentInChildren<LaserGun>().damage += 3f;
            GameObject.Find("Player").GetComponentInChildren<LaserGun>().oneTime = true;
        }
        updatePoint--;
        if(updatePoint <=0 ) gameObject.SetActive(false);
        else SetUpdateMenu();
    }

    public void RestoreHP()
    {
        Time.timeScale = 1;
        player.healthPoint += (player.GetMaxHealth()/2);
        updatePoint--;
        if( updatePoint <=0 ) gameObject.SetActive(false);
        else SetUpdateMenu();
    }

    public void IncreaseHP()
    {
        Time.timeScale = 1;
       
        float health = player.healthPoint + player.GetMaxHealth()*0.3f;
        player.SetMaxHealth(player.GetMaxHealth() + player.GetMaxHealth()*0.3f);
        player.healthPoint = health;
        updatePoint--;
        if(updatePoint <=0 ) gameObject.SetActive(false);
        else SetUpdateMenu();
    }

    public void DrainHP()
    {
        Time.timeScale = 1;
        player.isDrainHP = true;
        updatePoint--;
        if(updatePoint <=0 ) gameObject.SetActive(false);
        else SetUpdateMenu();
    }

    public void SlowEnemyBulletSpeed()
    {   
        Time.timeScale = 1;
        gameManager.isEnemyBulletSlowDown = true;
        updatePoint--;
        if( updatePoint <=0 ) gameObject.SetActive(false);
        else SetUpdateMenu();
    }
}
