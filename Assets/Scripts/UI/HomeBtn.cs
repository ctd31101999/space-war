﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HomeBtn : MonoBehaviour
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(Home);
    }
    
    void Home()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Main Menu");
    }
}
