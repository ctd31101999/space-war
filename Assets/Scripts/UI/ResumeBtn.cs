﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResumeBtn : MonoBehaviour
{
    public Text countdownText;
    public GameObject pauseMenu;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(Resume);
    }

    private void Resume()
    {
        Time.timeScale = 0.3f;
        // countdownText.gameObject.SetActive(true);
        pauseMenu.SetActive(false);
    }
}
