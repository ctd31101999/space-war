﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeUI : MonoBehaviour
{
    #region Singleton
    public static UpgradeUI Instance;
    void Awake()
    {
        Instance = this;
    }
    #endregion 
   
    public delegate void OnEquipmentUpgrade(Equipment newEquip, Equipment oldEquip);
    public OnEquipmentUpgrade onEquipmentUpgrade;
    public Text nameText;
    public Image icon;
    public Text levelText;
    public Text descriptionText;
    public Text statsText;
    public Text upgradeMaterialText;
    public Text equipButtonText;

    public Text informText;
    Equipment equipment;
    public void closeUpgradeUI()
    {
        setActive(false, null);
    }

    public void Equip()
    {
        if(!equipment.isEquiped)
        {
            Debug.Log("Equip: " + equipment.equipSlot);

            EquipmentManager.Instance.Equip(equipment);
            equipment.RemoveFromInventory();
        }
        else
        {
            Debug.Log("UnEquip: " + equipment.equipSlot);
            EquipmentManager.Instance.UnEquip((int)equipment.equipSlot);
        }
        closeUpgradeUI();
    }
    
    public void setActive(bool value, Equipment equip)
    {
        if(equip != null)
        {
            equipment = equip;        
            if(!equipment.isEquiped) 
            {
                equipButtonText.text = "Equip";
            }
            else
            {
                equipButtonText.text = "UnEquip";
            }
            parseData();
        }
       
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(value); 
        }
    }

    public void upgrade()
    {
        if(equipment != null)
        {
            if(GameObject.Find("Game Controller").GetComponent<CoinAndWeaponManage>().Coins < equipment.upgradeMaterial) 
            {
                informText.gameObject.SetActive(true);
            }
            else
            {
                GameObject.Find("Game Controller").GetComponent<CoinAndWeaponManage>().Coins -= equipment.upgradeMaterial;
                GameObject.Find("Game Controller").GetComponent<CoinAndWeaponManage>().user.coins -= equipment.upgradeMaterial;
                GameObject.Find("Game Controller").GetComponent<CoinAndWeaponManage>().user.SaveUserState();

                equipment.Upgrade();
                parseData(); 
                if(onEquipmentUpgrade != null) onEquipmentUpgrade.Invoke(null, equipment);
            }   
        }
    }

    void parseData()
    {
        informText.gameObject.SetActive(false);
        icon.sprite = equipment.icon;
        nameText.text = equipment.name;
        descriptionText.text = equipment.description;
        levelText.text = equipment.Level.ToString() + "/50";
        statsText.text = "armorModifier +" + equipment.armorModifier + "\n" + "damageModifier +" + equipment.damageModifier + "\n" + "hpModifier +" + equipment.hpModifier;
        upgradeMaterialText.text = equipment.upgradeMaterial.ToString();
    }
}
