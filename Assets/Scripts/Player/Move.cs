﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Move : MonoBehaviour
{
    [SerializeField] Button move1Button;
    [SerializeField] Button move2Button;
    public GameObject pauseMenu;

    public delegate void Controller();

    Controller controller;
    private Touch touch;
    private float speedModifier;
    private const float epsilon = 0.01f;

    void Start()
    {
        if(GameObject.Find("Game Controller").GetComponent<GameManager>().typeMove)
        {
            ChangeMove1();
        }
        else
        {
            ChangeMove2();
        }
        speedModifier = 0.01f;
    }

    void Update()
    {
        controller();
    }

    void Move1()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase.Equals(TouchPhase.Moved) && Time.timeScale > 0f)
            {
                Time.timeScale = 1f;
                transform.position = new Vector3(
                    transform.position.x + touch.deltaPosition.x * speedModifier,
                    transform.position.y + touch.deltaPosition.y * speedModifier,
                    transform.position.z
                );
            }
        }
    }
    float acumTime = 0;
    void Move2()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            acumTime += Time.deltaTime;
 
            if(acumTime >= 0.09f && Time.timeScale > 0f)
            {
                Time.timeScale = 1f;
                float newX = (10.8f * touch.position.x / Screen.width - 5.4f);
                float newY = (19.2f * touch.position.y / Screen.height - 9.6f);
                transform.position = new Vector3(
                    transform.position.x + (newX - transform.position.x) * speedModifier * 20f,
                    transform.position.y + (newY + 2f - transform.position.y) * speedModifier * 20f,
                    transform.position.z
                );
            }
    
            if(touch.phase == TouchPhase.Ended)
            {
                acumTime = 0; 
            }
        }
    }

    public void ChangeMove1()
    {
        controller = Move1;
        GameObject.Find("Game Controller").GetComponent<GameManager>().typeMove = true;
        move1Button.interactable = false;
        move2Button.interactable = true;
        move1Button.transform.Find("Selected").gameObject.SetActive(true);
        move2Button.transform.Find("Selected").gameObject.SetActive(false);
    }

    public void ChangeMove2()
    {
        controller = Move2;
        GameObject.Find("Game Controller").GetComponent<GameManager>().typeMove = false;
        move1Button.interactable = true;
        move2Button.interactable = false;
        move1Button.transform.Find("Selected").gameObject.SetActive(false);
        move2Button.transform.Find("Selected").gameObject.SetActive(true);
    }
}
