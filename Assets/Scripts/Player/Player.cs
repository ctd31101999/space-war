﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Ship
{
    public SU_Thruster thruster;
    public bool isDrainHP = false;
    public Camera cam;
    public Vector3 screenPoint;
    Animator animator;
    float positionX;
    float positionY;
    [SerializeField] List<Equipment> items;
    void Start()
    {
        screenPoint = cam.WorldToViewportPoint(gameObject.transform.position);
        positionX = gameObject.transform.position.x;
        positionY = gameObject.transform.position.y;
        animator = GetComponentInChildren<Animator>();
        foreach(Equipment x in items)
        {
            if(x.isEquiped)
            {
                transform.GetChild(items.IndexOf(x)).gameObject.SetActive(true);
                transform.GetChild(items.IndexOf(x)).GetComponent<Weapon>().damage = x.damageModifier;
                SetMaxHealth(x.hpModifier);
            }
            else
            {
                transform.GetChild(items.IndexOf(x)).gameObject.SetActive(false);
            }
        }
    }
    
    void Update()
    {
        Move();
        if(gameObject.transform.position.x > positionX) 
        {
            animator.SetBool("IsMovingLeft", false);
            animator.SetBool("IsMovingRight", true);
        } else if (gameObject.transform.position.x < positionX)
        {
            animator.SetBool("IsMovingRight", false);
            animator.SetBool("IsMovingLeft", true);
        } else if(gameObject.transform.position.x == positionX)
        {
            animator.SetBool("IsMovingRight", false);
            animator.SetBool("IsMovingLeft", false);
        }
        if(gameObject.transform.position.y > positionY)
        {
            thruster.StartThruster();
        }
        else
        {
            thruster.StopThruster();
        }
        positionX = gameObject.transform.position.x;
        positionY = gameObject.transform.position.y;
    }

    public override void Move()
    {
        screenPoint = cam.WorldToViewportPoint(gameObject.transform.position);
        bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
        
        if(screenPoint.x>0.95f) 
        {
            gameObject.transform.position = new Vector3(4.9f, gameObject.transform.position.y, gameObject.transform.position.z);
        } 
        else if(screenPoint.x<0.05f)
        {
            gameObject.transform.position = new Vector3(-4.9f, gameObject.transform.position.y, gameObject.transform.position.z);
        }
        if(screenPoint.y<0.05f) 
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, -8.65f, gameObject.transform.position.z);
        } 
        else if(screenPoint.y>0.95f)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, 8.65f, gameObject.transform.position.z);         
        }
    }
}

