﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User : MonoBehaviour
{
    public int currentLevel;
    public int coins;
    public Equipment[] equipmentList;

    public void SaveUserState()
    {
        SaveSystem.SaveUser(this);
    }

    public void LoadUserState()
    {
        UserData data = SaveSystem.LoadUser();

        currentLevel = data.currentLevel;
        coins = data.coins;
    }
}
