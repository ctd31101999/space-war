﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserData
{
    public int currentLevel;
    public int coins;
    // public Equipment[] equipmentList;

    public UserData(User user)
    {
        currentLevel = user.currentLevel;
        coins = user.coins;
    }
}
