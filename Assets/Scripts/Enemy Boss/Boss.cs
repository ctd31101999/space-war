﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Boss : Ship
{
    public ParticleSystem exploseParticale;
    public int wave;
    public bool oneTime = true;
    static bool oneTimeOfHeath = true;
    Vector3 destination = new Vector3(0f, 13f, 18f);
    float timeCount = 3f;
    float countdown = 1f;
    bool one = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!transform.position.Equals(destination))
        {
            Move();
        }
        else
        {
            switch(wave)
            {
                case 0:
                {
                    changeBullet();
                    break;
                }
                case 1:
                {
                    if(timeCount <= 0)
                    {
                        wave++;
                        oneTime = true;
                    }
                    else
                    {
                        timeCount -= Time.deltaTime;
                    }
                    if(countdown <= 0)
                    {
                        if(one)
                        {
                            for(int j = 0; j < 3; j++)
                            {
                                listWeapon[1].transform.GetChild(j).GetComponent<LaserGun>().wait = false;
                                listWeapon[1].transform.GetChild(j).GetComponent<LaserGun>().oneTime = true;
                            }
                            one = false;
                        }
                    }
                    else
                    {
                        countdown -= Time.deltaTime;
                        if(one)
                        {
                            listWeapon[0].gameObject.SetActive(false);
                            listWeapon[1].gameObject.SetActive(true);
                            listWeapon[2].gameObject.SetActive(false);
                            for(int j = 0; j < 3; j++)
                            {
                                listWeapon[1].transform.GetChild(j).GetComponent<LaserGun>().wait = true;
                                listWeapon[1].transform.GetChild(j).GetComponent<LaserGun>().oneTime = true;
                            }
                            one = false;
                        }
                        if(countdown <= 0)
                        {
                            one = true;
                        }
                    }
                    break;
                }
                case 2:
                {
                    changeBullet();
                    break;
                }
            }
        }  
    }
    public override void Move()
    {
        if( Mathf.Abs( destination.x - transform.position.x ) < 0.1f )
        {
            transform.position = new Vector3(destination.x, transform.position.y, transform.position.z);
        }
        if( Mathf.Abs( destination.y - transform.position.y ) < 0.1f )
        {
            transform.position = new Vector3(transform.position.x, destination.y, transform.position.z);
        }
        Vector3 lookVector = destination - transform.position;
        if(lookVector.Equals(Vector3.zero))
        {
            foreach( Collider x in GetComponents<Collider>() )
                {
                    x.enabled = true;
                }
            transform.GetChild(4).gameObject.SetActive(true);
        }
        transform.position += lookVector.normalized*5f*Time.deltaTime;
    }
    void changeBullet()
    {
        if(oneTime)
        {
            for(int i = 0; i < 3 ;i++)
            {
                if(i == wave) 
                {
                    listWeapon[i].gameObject.SetActive(true);
                }
                else
                {
                    listWeapon[i].gameObject.SetActive(false);
                }
            }
            oneTime = false;
        }
    }
    public override void ReceiveDamage(float damage)
    {
        base.ReceiveDamage(damage);
        if( GetComponentInChildren<Slider>().value <= 0.55*GetMaxHealth() && oneTimeOfHeath )
        {
            oneTimeOfHeath = false;
            wave++;
            oneTime = true;
        }
        if( GetComponentInChildren<Slider>().value <= 0 && transform.tag.Equals("Enemy") )
        {
            ParticleSystem explosion = Instantiate(exploseParticale, transform.position, Quaternion.Euler(90.0f, 0.0f, 0.0f));
            explosion.transform.localScale = new Vector3(3f, 3f, 3f);
            Destroy(transform.gameObject);
        }
    }
}
