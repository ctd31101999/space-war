﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class Enemy : Ship
{
    public ParticleSystem exploseParticale;
    public static UIManager uIManager;
    public static GameManager gameManager;
    public float moveSpeed;
    Transform target;
    bool onScreen = false;
    public Vector3 position;
    [SerializeField] GameObject coinPrefab;
    public static GameObject gameController;
    public Vector3 centerEllipse;
    public float ellipseHeight;
    public float ellipseWeight;
    public float angle = 0.0f;
    Vector3 movementVector;
    ExpBar expBar;
    public bool left = false;
    public bool suicide;
    public delegate void patternMove();
    public patternMove PatternMove;
    bool isDead = true;
    // Start is called before the first frame update
    void Start()
    {
        gameController = GameObject.Find("Game Controller");
        uIManager = gameController.GetComponent<UIManager>();
        gameManager = gameController.GetComponent<GameManager>();
        target = GameObject.Find("Player").transform;
        movementVector = lookAt(position);
    }

    // Update is called once per frame
    void Update()
    {
        PatternMove();
    }

    bool change = false;
    void IsChange()
    {
        if( Mathf.Abs( position.x - transform.position.x ) < 0.2f && transform.position.y - position.y < 0.2f )
        {
            change = true;
        }
    }
    public override void Move()
    {}
    public void TestMove()
    {
        if(change)
        {
            MoveEllipse(centerEllipse, ellipseHeight, ellipseWeight, true);
        }
        else
        {
            MoveToPoint(position);
            IsChange();
        }
    }

    public void TestMove1()
    {
        if(change) 
        {   
            if(suicide)
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            MoveToPoint(position);
            IsChange();
        }
    }

    public void TestMove2()
    {
        if(change) 
        {   
            MoveXAxis();
        }
        else
        {
            MoveToPoint(position);
            IsChange();
        }
    }

    public void TestMove3()
    {
        if(change) 
        {   
            if(suicide)
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            MoveSinLine(1.58f,7.9f);
            IsChange();
        }
    }

    public void TestMove4()
    {
        if(change) 
        {   
            Destroy(this.gameObject);
        }
        else
        {
            MoveToPoint(position);
            lookAt(target.position);
            IsChange();
        }
    }
    public override void ReceiveDamage(float damage)
    {
        base.ReceiveDamage(damage);

        if( GetComponentInChildren<Slider>().value <= 0 && isDead )
        {
            isDead = false;
            GameObject coin = coinPrefab.Spawn(transform.position, Quaternion.Euler(90.0f, 0.0f, 0.0f ), new Vector3(40.0f, 40.0f, 40.0f));
            coin.GetComponent<Coins>().value = UnityEngine.Random.Range(1, 5);
            coin.GetComponent<Coins>().tagObject = "Coin";
            GameObject.Find("Exp Bar").GetComponent<ExpBar>().IncreaseExp(2);
            Instantiate(exploseParticale, transform.position, Quaternion.Euler(90.0f, 0.0f, 0.0f));
            if(GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().isDrainHP) 
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().healthPoint += 10f;
            }
            Destroy(transform.gameObject);
        }
    }

    void MoveToPoint(Vector3 destination)
    {
        if( Mathf.Abs( transform.position.x - destination.x ) < 0.2f )
        {
            transform.position = new Vector3(destination.x, transform.position.y, transform.position.z);
        }
        if( transform.position.y - destination.y < 0.2f )
        {
            transform.position = new Vector3(transform.position.x, destination.y, transform.position.z);
        }
        Vector3 forward = movementVector.normalized;
        if(transform.position == destination)
        {
            forward = Vector3.zero;
        }
        transform.position += movementVector.normalized*moveSpeed*Time.deltaTime;
    }

    void MoveXAxis()
    {   
        if(!left)
        {
            transform.position += Vector3.right*2.0f*Time.deltaTime;
            if(transform.position.x > 4.0f) left = !left;
        }
        else
        {
            transform.position += Vector3.left*2.0f*Time.deltaTime;
            if(transform.position.x < -4.0f) left = !left;
        }
    }
    
    void MoveEllipse(Vector3 center, float height, float weight, bool follow = false)
    {
        Vector3 ellipseVector = transform.position - center;
        float newX = center.x + Mathf.Cos(angle*Mathf.Deg2Rad)*weight;
        float newY = center.y + Mathf.Sin(angle*Mathf.Deg2Rad)*height;
        angle = angle + (left?1:-1)*Time.deltaTime*90.0f;
        Vector3 destination = new Vector3(newX, newY, transform.position.z);
        if(follow)
        {
            lookAt(destination);
        }
        transform.position = destination;
    }

    Vector3 lookAt(Vector3 target)
    {
        Vector3 lookVector = target - transform.position;
        if( lookVector == Vector3.zero) return lookVector;
        Quaternion rot = Quaternion.LookRotation(lookVector, transform.position);
        float rotationX = (lookVector.x >= 0)? 1 : -1;
        float rotationY = (lookVector.y >= 0)? 450 : 90;
        if( Mathf.Abs(rotationY - rot.eulerAngles.x) > 5.0f)
        {
            rot = Quaternion.Euler(0.0f, 0.0f, rotationX*(rotationY - rot.eulerAngles.x));
        }
        else
        {
            rot = Quaternion.Euler(0.0f, 0.0f, 0.0f);
        }
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, 10.0f*Time.deltaTime);
        return lookVector;
    }
    bool up = false;
    void MoveSinLine(float min, float max)
    {
        if(!up && transform.position.y < min) up = true;
        if(up && transform.position.y > max) up = false;
        float newY = transform.position.y + (up?1:-1)*Time.deltaTime;
        float newX = (left?1:-1)*4.5f*Mathf.Sin(newY);
        Vector3 destination = new Vector3(newX, newY, transform.position.z);
        lookAt(target.position);
        transform.position = destination;
    }
}
