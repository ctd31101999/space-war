﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class UIManager : MonoBehaviour
{
    public int coins;
    public Canvas updateMenu;
    public Canvas gameOverMenu;
    public ExpBar expBar;
    List<Action> upgradeList;
    void Start()
    {
        Time.timeScale = 1;
    }
    void Update()
    {
        if(GameObject.Find("Player").GetComponentInChildren<Player>().healthPoint <= 0)
        {
            gameOverMenu.GetComponentInChildren<Text>().text = "GAME OVER";
            GameOver();
        }
    }

    public void GameOver()
    {
        gameOverMenu.gameObject.SetActive(true);
        Time.timeScale = 0;
    }
    public void update()
    {
        //Time.timeScale = 0;
        updateMenu.GetComponentInChildren<UpdateMenu>().updatePoint++;
        //updateMenu.gameObject.SetActive(true);
    }
    public void Home()
    {
        GameObject.Find("Game Controller").GetComponent<GameManager>().enabled = false;
        GameObject.Find("Game Controller").GetComponent<GameManager>().counter = 0;
        GameObject.Find("Game Controller").GetComponent<GameManager>().timeStart = 3f;
        GameObject.Find("Game Controller").GetComponent<GameManager>().timeFinish = 3f;
        Time.timeScale = 1;
        SceneManager.LoadScene("Select Level");
    }
    public void Play()
    {
        GameObject.Find("Game Controller").GetComponent<GameManager>().counter = 0;
        GameObject.Find("Game Controller").GetComponent<GameManager>().timeStart = 3f;
        GameObject.Find("Game Controller").GetComponent<GameManager>().timeFinish = 3f;
        Time.timeScale = 1;
        SceneManager.LoadScene("Main");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void SoundChange()
    {
        GetComponent<AudioSource>().mute = !GetComponent<AudioSource>().mute;
    }
}
