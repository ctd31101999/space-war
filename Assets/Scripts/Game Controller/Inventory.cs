﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Inventory : MonoBehaviour
{
    #region Singleton

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;
    public Transform weaponContain;
    public int space = 20;
    public static Inventory Instance;
    void Awake()
    {
        if(Instance != null)
        {
            Debug.LogWarning("More than 1 instance of inventory");
            return;
        }
        Instance = this;
    }
    #endregion
    public List<Item> items = new List<Item>();
    public bool Add(Item item)
    {
        if(!item.isDefaulItem)
        {
            if(items.Count >= space)
            {
                Debug.Log("Inventory out of slot");
                return false;
            }
            Debug.Log(item.name);
            items.Add(item);
            weaponContain.GetChild(items.IndexOf(item)).GetComponent<InventorySlot>().AddItime(item);
            if(onItemChangedCallback != null) onItemChangedCallback.Invoke();
        }

        return true;
    }

    public void Remove(Item item)
    {
        Debug.Log(items.IndexOf(item));
        weaponContain.GetChild(items.IndexOf(item)).GetComponent<InventorySlot>().ClearSlot();
        items.Remove(item);
        for(int i = 0 ; i < items.Count; i++)
        {
            weaponContain.GetChild(i).GetComponent<InventorySlot>().AddItime(items[i]);
        }
        for(int i = items.Count; i < space; i++)
        {
            weaponContain.GetChild(i).GetComponent<InventorySlot>().ClearSlot();
        }
        if(onItemChangedCallback != null) onItemChangedCallback.Invoke();
    }
}
