﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{   
    [SerializeField] List<Equipment> items;
    public GameObject[] enemy;
    public GameObject boss;
    public bool isEnemyBulletSlowDown = false;
    public int counter = 0;
    float timeSpawn;
    public float timeStart;
    public float timeFinish;
    public float timeWrap;
    public int level;
    public bool typeMove = true;
    void Start()
    {
        timeStart = 3f;
        timeFinish = 3f;
        timeWrap = 2f;
        Time.timeScale = 1;
    }

    void Update()
    {
        if(timeStart <= 0)
        {
            if( GameObject.FindWithTag("Enemy") == null )
            {   
                if(counter%10 == 7)
                {
                    timeFinish -= Time.deltaTime;
                    if(level == GetComponent<MainUIController>().currentLevel && level != 5)
                    {
                        GetComponent<MainUIController>().currentLevel++;
                        GetComponent<MainUIController>().selectLevel++;
                    }
                    if(timeFinish<=0)
                    {
                        GameObject.Find("Main Camera").GetComponent<UIManager>().gameOverMenu.GetComponentInChildren<Text>().text = "FINISH";
                        GameObject.Find("Main Camera").GetComponent<UIManager>().GameOver();
                    }
                    
                }
                else{
                    if(timeWrap <= 0f)
                    {
                        switch(level)
                        {
                            case 1:
                            {
                                level1();
                                break;
                            }
                            case 2:
                            {
                                level2();
                                break;
                            }
                            case 3:
                            {
                                level3();
                                break;
                            }
                            case 4:
                            {
                                level4();
                                break;
                            }
                            case 5:
                            {
                                level5();
                                break;
                            }
                        }
                        counter++;
                        timeWrap = 1f;
                    }
                    else
                    {
                        timeWrap -= Time.deltaTime;
                        Canvas updateMenu = GameObject.Find("Main Camera").GetComponent<UIManager>().updateMenu;
                        if(updateMenu.GetComponentInChildren<UpdateMenu>().updatePoint > 0 && timeWrap <= 0.5f)
                        {
                            updateMenu.gameObject.SetActive(true);
                        }
                    }
                }
            }
        }
        else
        {
            if(GameObject.Find("Tutorial") == null)
            {
                timeStart -= Time.deltaTime;
            }
        }
    }

    void level1()
    {
        switch(counter%10)
        {
            case 0: 
            {
                Spawn_1_1();
                break;
            }
            case 1:
            {
                Spawn_1_2();
                break;
            }
            case 2:
            {
                Spawn_1_3();
                break;
            }
            case 3:
            {
                Spawn_1_4();
                break;
            }
            case 4:
            {
                Spawn_1_5();
                break;
            }
            case 5:
            {
                Spawn_1_6();
                break;
            }
            case 6:
            {
                Spawn_1_7();
                break;
            }
        }
    }

    void level2()
    {
        switch(counter%10)
        {
            case 0: 
            {
                Spawn_2_1();
                break;
            }
            case 1:
            {
                Spawn_2_2();
                break;
            }
            case 2:
            {
                Spawn_2_3();
                break;
            }
            case 3:
            {
                Spawn_2_4();
                break;
            }
            case 4:
            {
                Spawn_2_5();
                break;
            }
            case 5:
            {
                Spawn_2_6();
                break;
            }
            case 6:
            {
                Spawn_2_7();
                break;
            }
        }
    }
   
    void level3()
    {
        switch(counter%10)
        {
            case 0: 
            {
                Spawn_3_1();
                break;
            }
            case 1:
            {
                Spawn_3_2();
                break;
            }
            case 2:
            {
                Spawn_3_3();
                break;
            }
            case 3:
            {
                Spawn_3_4();
                break;
            }
            case 4:
            {
                Spawn_3_5();
                break;
            }
            case 5:
            {
                Spawn_3_6();
                break;
            }
            case 6:
            {
                Spawn_3_7();
                break;
            }
        }
    }

    void level4()
    {
        switch(counter%10)
        {
            case 0: 
            {
                Spawn_4_1();
                break;
            }
            case 1:
            {
                Spawn_4_2();
                break;
            }
            case 2:
            {
                Spawn_4_3();
                break;
            }
            case 3:
            {
                Spawn_4_4();
                break;
            }
            case 4:
            {
                Spawn_4_5();
                break;
            }
            case 5:
            {
                Spawn_4_6();
                break;
            }
            case 6:
            {
                Spawn_4_7();
                break;
            }
        }    
    }
   void level5()
   {
       switch(counter%10)
        {
            case 0: 
            {
                Spawn_5_1();
                break;
            }
            case 1:
            {
                Spawn_5_2();
                break;
            }
            case 2:
            {
                Spawn_5_3();
                break;
            }
            case 3:
            {
                Spawn_5_4();
                break;
            }
            case 4:
            {
                Spawn_5_5();
                break;
            }
            case 5:
            {
                Spawn_5_6();
                break;
            }
            case 6:
            {
                GameObject Enemy = Instantiate(boss, new Vector3(0f, 22f, 18f), Quaternion.identity) as GameObject;
                break;
            }
        }
   }
    void SpawnEnemy1(   float health, 
                        Vector3 start, 
                        Vector3 positionChange, 
                        Vector3 centerEllipse, 
                        float angle = 0f, 
                        bool left = false, 
                        float speed = 5.0f  
                    )
    {
        GameObject Enemy = Instantiate(enemy[0], start, Quaternion.identity) as GameObject;
        Enemy.GetComponent<Enemy>().SetMaxHealth(health);
        Enemy.GetComponent<Enemy>().moveSpeed = speed;
        Enemy.GetComponent<Enemy>().PatternMove = Enemy.GetComponent<Enemy>().TestMove;

        Enemy.GetComponent<Enemy>().position = positionChange;
        Enemy.GetComponent<Enemy>().centerEllipse = centerEllipse;
        Enemy.GetComponent<Enemy>().ellipseHeight = Mathf.Abs( positionChange.x - centerEllipse.x );
        Enemy.GetComponent<Enemy>().ellipseWeight = Mathf.Abs( positionChange.x - centerEllipse.x );
        Enemy.GetComponent<Enemy>().left = left;
        Enemy.GetComponent<Enemy>().angle = angle;

        Enemy.GetComponentInChildren<FollowGun>().fireRate = UnityEngine.Random.Range(0.1f, 0.2f);
        if(isEnemyBulletSlowDown) 
        {
            Enemy.GetComponentInChildren<Weapon>().bulletSpeed *= 0.8f;
        }
    }

    void SpawnEnemy2(   float health, 
                        Vector3 start, 
                        Vector3 positionChange,
                        float speed = 5.0f
                    )
    {
        GameObject Enemy = Instantiate(enemy[2], start, Quaternion.identity) as GameObject;
        Enemy.GetComponent<Enemy>().SetMaxHealth(health);
        Enemy.GetComponent<Enemy>().moveSpeed = speed;
        Enemy.GetComponent<Enemy>().PatternMove = Enemy.GetComponent<Enemy>().TestMove4;

        Enemy.GetComponent<Enemy>().position = positionChange;
        if(isEnemyBulletSlowDown) 
        {
            Enemy.GetComponentInChildren<Weapon>().bulletSpeed *= 0.8f;
        }
    }

    void SpawnEnemy3(   float health, 
                        Vector3 start, 
                        Vector3 positionChange, 
                        bool suicide = false, 
                        float speed = 5.0f 
                    )
    {
        GameObject Enemy = Instantiate(enemy[1], start, Quaternion.identity) as GameObject;
        Enemy.GetComponent<Enemy>().SetMaxHealth(health);
        Enemy.GetComponent<Enemy>().suicide = suicide;
        Enemy.GetComponent<Enemy>().PatternMove = Enemy.GetComponent<Enemy>().TestMove1;
        Enemy.GetComponent<Enemy>().moveSpeed = speed;
        Enemy.GetComponent<Enemy>().position = positionChange;
        if(isEnemyBulletSlowDown) 
        {
            Enemy.GetComponentInChildren<Weapon>().bulletSpeed *= 0.8f;
        }
    }

    void SpawnEnemy4(   float health, 
                        Vector3 start, 
                        Vector3 positionChange,
                        bool left = false,
                        bool suicide = false, 
                        float speed = 5.0f
                    )
    {
        GameObject Enemy = Instantiate(enemy[3], start, Quaternion.identity) as GameObject;
        Enemy.GetComponent<Enemy>().SetMaxHealth(health);
        Enemy.GetComponent<Enemy>().suicide = suicide;
        Enemy.GetComponent<Enemy>().PatternMove = Enemy.GetComponent<Enemy>().TestMove2;
        Enemy.GetComponent<Enemy>().moveSpeed = speed;
        Enemy.GetComponent<Enemy>().position = positionChange;
        Enemy.GetComponent<Enemy>().left = left;
        if(isEnemyBulletSlowDown) 
        {
            Enemy.GetComponentInChildren<Weapon>().bulletSpeed *= 0.8f;
        }
    }

    void SpawnEnemy5(   float health, 
                        Vector3 start, 
                        Vector3 positionChange,
                        bool left = false,
                        bool suicide = false, 
                        float speed = 5.0f
                    )
    {
        GameObject Enemy = Instantiate(enemy[4], start, Quaternion.identity) as GameObject;
        Enemy.GetComponent<Enemy>().SetMaxHealth(health);
        Enemy.GetComponent<Enemy>().position = positionChange;
        Enemy.GetComponent<Enemy>().PatternMove = Enemy.GetComponent<Enemy>().TestMove3;
        Enemy.GetComponent<Enemy>().moveSpeed = speed;
        Enemy.GetComponent<Enemy>().left = left;
        if(isEnemyBulletSlowDown) 
        {
            Enemy.GetComponentInChildren<Weapon>().bulletSpeed *= 0.8f;
        }
    }

    void Spawn_1_1()
    {
        for(int i = 0 ; i < 7; i++)
        {
            SpawnEnemy1(40f,  new Vector3(-2.0f, 11.0f + 2.5f*i, 15.0f), new Vector3(-2.0f, 3.0f, 15.0f), new Vector3(0.0f, 3.0f, 15.0f), 180f, true);
        }
    }

    void Spawn_1_2()
    {
        for(int i = 0 ; i < 7; i++)
        {
            SpawnEnemy1(40f,  new Vector3(2.0f, 11.0f + 2.5f*i, 15.0f), new Vector3(2.0f, 3.0f, 15.0f), new Vector3(0.0f, 3.0f, 15.0f));
        }
    }

    void Spawn_1_3()
    {
        for(int i = 0 ; i < 3; i++)
        {
            SpawnEnemy2(120f, new Vector3(-4.0f + 4.0f*i, 12.0f + Mathf.Abs(-2.0f + 2.0f*i), 15.0f), new Vector3(-2.0f + 2.0f*i, -11.0f, 15.0f), 1f);
        }
    }

    void Spawn_1_4()
    {
        for(int i = 0 ; i < 7; i++)
        {
            SpawnEnemy1(40f,  new Vector3(0f, 11.0f + 2.0f*i, 15.0f), new Vector3(0f, 3.0f, 15.0f), new Vector3(4f, 3.0f, 15.0f), 180f, true);
        }
    }

    void Spawn_1_5()
    {
        float [] delta = {1f, 5f, 3f, 2f, 4f};
        for(int i = 0 ; i < 5; i++)
        {
            SpawnEnemy2(120f, new Vector3(-4.0f + 2.0f*i, 11.0f + delta[i], 15.0f), new Vector3(-4.0f + 2.0f*i, -11.0f, 15.0f), 1f);
        }
    }

    void Spawn_1_6()
    {
        for(int i = 0 ; i < 3; i++)
        {
            for(int j = 0 ; j < 2; j++)
            {
                SpawnEnemy2(120f, new Vector3(-4.0f + 4.0f*i, 11.0f + 3f*j, 15.0f), new Vector3(-4.0f + 4.0f*i, -11.0f, 15.0f), 1f);
            }
        }
    }

    void Spawn_1_7()
    {
        for(int i = 0; i < 2; i++ )
        {
            SpawnEnemy1(40f, new Vector3(-4f, 11f + 4f*i, 15f), new Vector3(-4f, 3f*i, 15f), new Vector3(-2f, 3f*i, 15f), 180f, true);
            SpawnEnemy1(40f, new Vector3(-4f, 13f + 4f*i, 15f), new Vector3(-4f, 3f*i, 15f), new Vector3(-2f, 3f*i, 15f), 180f, true);
            SpawnEnemy1(40f, new Vector3(4f, 11f + 4f*i, 15f), new Vector3(4f, 3f*i, 15f), new Vector3(2f, 3f*i, 15f));
            SpawnEnemy1(40f, new Vector3(4f, 13f + 4f*i, 15f), new Vector3(4f, 3f*i, 15f), new Vector3(2f, 3f*i, 15f));
            SpawnEnemy2(120f, new Vector3(-2.0f, 13.0f + 2f*i, 15.0f), new Vector3(-2.0f, -11.0f, 15.0f), 1f);
            SpawnEnemy2(120f, new Vector3(2.0f, 13.0f + 2f*i, 15.0f), new Vector3(2.0f, -11.0f, 15.0f), 1f);
        }
    }

    void Spawn_2_1()
    {
        for(int i = 0 ; i < 7; i++)
        {
            SpawnEnemy1(40f,  new Vector3(0f, 12.0f + 2.0f*i, 15.0f), new Vector3(0f, 3.0f, 15.0f), new Vector3(4f, 3.0f, 15.0f), 180f, true);
        }
    }

    void Spawn_2_2()
    {
        for(int i = 0 ; i < 7; i++)
        {
            SpawnEnemy1(40f,  new Vector3(0f, 12.0f + 2.0f*i, 15.0f), new Vector3(0f, 3.0f, 15.0f), new Vector3(-4f, 3.0f, 15.0f));
        }
    }

    void Spawn_2_3()
    {
        for(int i = 0 ; i < 2; i++)
        {
            for(int j = 0 ; j < 2; j++)
            {
                SpawnEnemy2(120f, new Vector3(-4.0f + 8.0f*i, 12.0f + 4f*j, 15.0f), new Vector3(-4.0f + 8.0f*i, -11.0f, 15.0f), 1f);
            }
        }
    }

    void Spawn_2_4()
    {
        for(int i = 0 ; i < 2; i++)
        {
            SpawnEnemy3(240f, new Vector3(-2.5f, 11.0f + 4f*i, 15.0f), new Vector3(-2.5f, -12.0f, 15.0f), true, 1.5f);
            SpawnEnemy3(240f, new Vector3(2.5f, 11.0f + 4f*i, 15.0f), new Vector3(2.5f, -12.0f, 15.0f), true, 1.5f);
        }
    }

    void Spawn_2_5()
    {
        for(int j = 0; j < 4; j++)
        {
            SpawnEnemy1(40f, new Vector3(-4f , 11.0f + 2.0f*j, 15.0f), new Vector3(-4f, 3.0f, 15.0f), new Vector3(-2f, 3.0f, 15.0f), 180f, true);
            SpawnEnemy1(40f, new Vector3(4f , 11.0f + 2.0f*j, 15.0f), new Vector3(4f, 3.0f, 15.0f), new Vector3(2f, 3.0f, 15.0f));
        }
        SpawnEnemy3(240f, new Vector3(0f, 15.0f, 15.0f), new Vector3(0f, -12.0f, 15.0f), true, 1.5f);
    }

    void Spawn_2_6()
    {
        for(int i = 0; i < 2; i++)
        {
            SpawnEnemy2(120f, new Vector3(4f, 11.0f + 4f*i, 15.0f), new Vector3(0f, -11.0f, 15.0f), 1.5f);
            SpawnEnemy2(120f, new Vector3(-4f, 11.0f + 4f*i, 15.0f), new Vector3(0f, -11.0f, 15.0f), 1.5f);
            SpawnEnemy3(240f, new Vector3(-4f + 8f*i, 13.0f, 15.0f), new Vector3(0f, -12.0f, 15.0f), true, 1.5f);
        }
    }

    void Spawn_2_7()
    {
        for(int i = 0 ; i < 3; i++)
        {
            SpawnEnemy3(240f, new Vector3(-3f + 3f*i, 11.0f + Mathf.Abs(1f-i), 15.0f), new Vector3(-3f + 3f*i, -12.0f, 15.0f), true, 1.5f);
        }
        for(int i = 0 ; i < 3; i++)
        {
            SpawnEnemy2(120f, new Vector3(-4f + 4f*i, 13.0f + 2f*Mathf.Abs(1f-i), 15.0f), new Vector3(-4f + 4f*i, -11.0f, 15.0f), 1.5f);
        }
    }

    void Spawn_3_1()
    {
            for(int i = 0 ; i < 7; i++)
            {
                SpawnEnemy1(40f,  new Vector3(-2.0f, 11.0f + 2.5f*i, 15.0f), new Vector3(-2.0f, 3.0f, 15.0f), new Vector3(1.0f, 3.0f, 15.0f), 180f, true);
            }
    }

    void Spawn_3_2()
    {
        for(int i = 0 ; i < 2; i++)
        {
            SpawnEnemy3(240f, new Vector3(-3f + 6f*i, 11.0f + Mathf.Abs(1f-i), 15.0f), new Vector3(-3f + 6f*i, -12.0f, 15.0f), true, 1.5f);
        }
        for(int j = 0; j < 2; j++)
        {
            SpawnEnemy1(40f, new Vector3(-4f , 11.0f + 2.0f*j, 15.0f), new Vector3(-4f, 3.0f, 15.0f), new Vector3(-2f, 3.0f, 15.0f), 180f, true);
            SpawnEnemy1(40f, new Vector3(4f , 11.0f + 2.0f*j, 15.0f), new Vector3(4f, 3.0f, 15.0f), new Vector3(2f, 3.0f, 15.0f));
        }
    }

    void Spawn_3_3()
    {
        for(int i = 0 ; i < 7; i++)
        {
            SpawnEnemy1(40f,  new Vector3(2.0f, 11.0f + 2.5f*i, 15.0f), new Vector3(2.0f, 3.0f, 15.0f), new Vector3(-1.0f, 3.0f, 15.0f));
        }
    }

    void Spawn_3_4()
    {
        SpawnEnemy4(360f, new Vector3(-4.0f, 15.0f, 15.0f), new Vector3(-4.0f, 7.0f, 15.0f));
    }

    void Spawn_3_5()
    {
        for(int j = 0; j < 7; j++)
        {
            SpawnEnemy1(40f, new Vector3(-4f , 11.0f + 2.0f*j, 15.0f), new Vector3(-4f, 3.0f, 15.0f), new Vector3(-2f, 3.0f, 15.0f), 180f, true);
            SpawnEnemy1(40f, new Vector3(4f , 11.0f + 2.0f*j, 15.0f), new Vector3(4f, 3.0f, 15.0f), new Vector3(2f, 3.0f, 15.0f));
        }
    }

    void Spawn_3_6()
    {
        SpawnEnemy4(360f, new Vector3(-4.0f, 15.0f, 15.0f), new Vector3(-4.0f, 7.0f, 15.0f));
        for(int i = 0 ; i < 3; i++)
        {
            SpawnEnemy3(240f, new Vector3(-3f + 3f*i, 11.0f + Mathf.Abs(1f-i), 15.0f), new Vector3(-3f + 3f*i, -12.0f, 15.0f), true, 1.8f);
        }
    }

    void Spawn_3_7()
    {
        SpawnEnemy4(360f, new Vector3(0f, 15.0f, 15.0f), new Vector3(0f, 7.0f, 15.0f), false, false, 2f);
        SpawnEnemy3(240f, new Vector3(2.0f , 12.0f, 15.0f), new Vector3(2f, -11.0f, 15.0f), true, 1.5f);
        SpawnEnemy3(240f, new Vector3(-2.0f , 12.0f, 15.0f), new Vector3(-2f, -11.0f, 15.0f), true, 1.5f);
        for(int i = 0; i < 4; i++)
        {
            SpawnEnemy1(40f,  new Vector3(4.0f, 11.0f + 4.0f*i, 15.0f), new Vector3(4.0f, 3.0f, 15.0f), new Vector3(0.0f, 3.0f, 15.0f));
            SpawnEnemy1(40f,  new Vector3(-4.0f, 11.0f + 4.0f*i, 15.0f), new Vector3(-4.0f, 3.0f, 15.0f), new Vector3(0.0f, 3.0f, 15.0f), 180f, true);
        }
    }

    void Spawn_4_1()
    {
        for(int i = 0 ; i < 10; i++)
        {
            SpawnEnemy1(40f,  new Vector3(-4.0f, 12.0f + 2.0f*i, 15.0f), new Vector3(-4.0f, 3.0f, 15.0f), new Vector3(0.0f, 3.0f, 15.0f), 180f, true);
        } 
    }

    void Spawn_4_2()
    {
        for(int i = 0 ; i < 5; i++)
        {
            SpawnEnemy1(40f,  new Vector3(-2.0f, 11.0f + 2.5f*i, 15.0f), new Vector3(-2.0f, 3.0f, 15.0f), new Vector3(2.0f, 3.0f, 15.0f), 180f, true);
            SpawnEnemy1(40f,  new Vector3(2.0f, 11.0f + 2.5f*i, 15.0f), new Vector3(2.0f, 3.0f, 15.0f), new Vector3(-2.0f, 3.0f, 15.0f));
        }
    }

    void Spawn_4_3()
    {
        for(int i = 0; i < 2; i++)
        {
            SpawnEnemy2(120f, new Vector3(-4.0f + 8f*i, 15.0f, 15.0f), new Vector3(-4.0f + 8f*i, -11.0f, 15.0f), 1.5f);
            SpawnEnemy2(120f, new Vector3(-2.0f + 4f*i, 11.0f, 15.0f), new Vector3(-2.0f + 4f*i, -11.0f, 15.0f), 1.5f);
        }
    }

    void Spawn_4_4()
    {
        SpawnEnemy5(520f, new Vector3(4f, 14.0f, 15.0f), new Vector3(0f, 11.0f, 15.0f), true);
    }

    void Spawn_4_5()
    {
        for(int i = 0 ; i < 5; i++)
        {
            SpawnEnemy1(40f,  new Vector3(-4.0f, 12.0f + 2.0f*i, 15.0f), new Vector3(-4.0f, 3.0f, 15.0f), new Vector3(0.0f, 3.0f, 15.0f), 180f, true);
        }
        SpawnEnemy5(520f, new Vector3(4f, 14.0f, 15.0f), new Vector3(0f, 11.0f, 15.0f), true);
    }

    void Spawn_4_6()
    {
        SpawnEnemy5(520f, new Vector3(4f, 14.0f, 15.0f), new Vector3(0f, 11.0f, 15.0f), true);
        SpawnEnemy5(520f, new Vector3(-4f, 14.0f, 15.0f), new Vector3(0f, 11.0f, 15.0f));
    }

    void Spawn_4_7()
    {
        SpawnEnemy5(520f, new Vector3(-4f, 14.0f, 15.0f), new Vector3(0f, 11.0f, 15.0f));
        for(int i=0; i<3; i++)
        {
            SpawnEnemy2(120f, new Vector3(-4.0f + 4f*i , 15.0f - Mathf.Abs(2f-2f*i) , 15.0f), new Vector3(-4.0f + 4f*i, -11.0f, 15.0f), 1f);
        }
    }

    void Spawn_5_1()
    {
        for(int i = 0 ; i < 10; i++)
        {
            SpawnEnemy1(40f,  new Vector3(-4.0f, 12.0f + 2.0f*i, 15.0f), new Vector3(-4.0f, 3.0f, 15.0f), new Vector3(0.0f, 3.0f, 15.0f), 180f, true);
        }
    }

    void Spawn_5_2()
    {
        SpawnEnemy4(360f, new Vector3(-3.0f, 11.0f, 15.0f), new Vector3(-3.0f, 7.0f, 15.0f));
        SpawnEnemy4(360f, new Vector3(3.0f, 11.0f, 15.0f), new Vector3(3.0f, 4.5f, 15.0f), true);
    }

    void Spawn_5_3()
    {
        for(int i = 0 ; i < 10; i++)
        {
            SpawnEnemy1(40f,  new Vector3(4.0f, 12.0f + 2.0f*i, 15.0f), new Vector3(4.0f, 3.0f, 15.0f), new Vector3(0.0f, 3.0f, 15.0f));
        }
    }

    void Spawn_5_4()
    {
        SpawnEnemy5(520f, new Vector3(-3.0f, 11.0f, 15.0f), new Vector3(-3.0f, 11.0f, 15.0f), true);
        SpawnEnemy5(520f, new Vector3(3.0f, 11.0f, 15.0f), new Vector3(-3.0f, 11.0f, 15.0f));
    }

    void Spawn_5_5()
    {
        for(int i = 0 ; i < 7; i++)
        {
            SpawnEnemy1(40f,  new Vector3(1f, 12.0f + 2.0f*i, 15.0f), new Vector3(1f, 3.0f, 15.0f), new Vector3(4f, 3.0f, 15.0f), 180f, true);
        }
        for(int i = 0 ; i < 7; i++)
        {
            SpawnEnemy1(40f,  new Vector3(-1f, 12.0f + 2.0f*i, 15.0f), new Vector3(-1f, 3.0f, 15.0f), new Vector3(-4f, 3.0f, 15.0f));
        }
    }

    void Spawn_5_6()
    {
        SpawnEnemy4(360f, new Vector3(0f, 12.0f, 15.0f), new Vector3(0f, 7.0f, 15.0f), false, false, 2f);
        SpawnEnemy5(520f, new Vector3(4f, 14.0f, 15.0f), new Vector3(0f, 11.0f, 15.0f), true);
        SpawnEnemy5(520f, new Vector3(-4f, 14.0f, 15.0f), new Vector3(0f, 11.0f, 15.0f));
    }
}
