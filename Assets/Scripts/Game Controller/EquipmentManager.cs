﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour
{
    #region Singleton
    public static EquipmentManager Instance;
    void Awake()
    {
        Instance = this;
        int numSlots = System.Enum.GetNames(typeof(EquipSlot)).Length;
        currentEquipment = new Equipment[numSlots];
    }
    #endregion

    public delegate void OnEquipmentChanged(Equipment newEquip, Equipment oldEquip);
    public OnEquipmentChanged onEquipmentChanged;
    public Equipment[] currentEquipment;

    void Start()
    {
        
    }

    public void Equip(Equipment newEquip)
    {
        int slotIndex = (int)newEquip.equipSlot;

        Equipment oldEquip = null;
        //Debug.Log(slotIndex);
        if(currentEquipment[slotIndex] != null)
        {
            oldEquip = currentEquipment[slotIndex];
            Inventory.Instance.Add(oldEquip);
            oldEquip.isEquiped =false;
        }
        currentEquipment[slotIndex] = newEquip;
        newEquip.isEquiped = true;
        if(onEquipmentChanged != null) onEquipmentChanged.Invoke(newEquip, oldEquip);
    }

    public void UnEquip(int slotIndex)
    {
        if(currentEquipment[slotIndex] != null)
        {
            Equipment oldEquip = currentEquipment[slotIndex];
            Inventory.Instance.Add(oldEquip);

            oldEquip.isEquiped = false;

            currentEquipment[slotIndex] = null;

            if(onEquipmentChanged != null)
            {
                onEquipmentChanged.Invoke(null, oldEquip);
            }
        }
    }

    public void UnEquipAll()
    {
        for(int i = 0; i < currentEquipment.Length; i++)
        {
            UnEquip(i);
        }
    }
}
