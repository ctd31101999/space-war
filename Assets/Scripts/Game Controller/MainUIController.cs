﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MainUIController : MonoBehaviour
{
    public int currentLevel;
    public int numberLevel;
    public int selectLevel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for(int i = 1; i <= currentLevel; i++)
        {
            GameObject.Find("Level " + i.ToString()).GetComponent<Image>().color = Color.white;
        }
        GameObject.Find("Level " + selectLevel.ToString()).GetComponent<Image>().color = Color.yellow;
        for(int i = currentLevel + 1;i <= numberLevel; i++)
        {
            GameObject.Find("Level " + i.ToString()).GetComponent<Image>().color = Color.gray;
            GameObject.Find("Level " + i.ToString()).GetComponent<Button>().enabled = false;
        }
    }
}
