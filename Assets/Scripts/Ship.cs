﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public abstract class Ship : MonoBehaviour
{

    public float healthPoint
    {
        get
        {
            if(GetComponentInChildren<Slider>() == null) return GameObject.Find("Full Health Bar").GetComponentInChildren<Slider>().value;
            return GetComponentInChildren<Slider>().value;
        }
        set
        {
            if(GetComponentInChildren<Slider>() == null) 
            {
                GameObject.Find("Full Health Bar").GetComponentInChildren<Slider>().value = value;
            }
            else GetComponentInChildren<Slider>().value = value;
        }
    }
    public float defense;
    public float attackDamage;
    public int coins;
    public List<GameObject> listWeapon = new List<GameObject>();
    public abstract void Move();
    public virtual void ReceiveDamage(float damage)
    {
        if(GetComponentInChildren<Slider>() == null) 
        {
            GameObject.Find("Full Health Bar").GetComponentInChildren<Slider>().value -= damage;
        }
        else GetComponentInChildren<Slider>().value -= damage;
    }

    public void SetMaxHealth(float health)
    {
        if(GetComponentInChildren<Slider>() == null) 
        {
            GameObject.Find("Full Health Bar").GetComponentInChildren<Slider>().maxValue = health;
        }else GetComponentInChildren<Slider>().maxValue = health;
        healthPoint = health;
    }

    public float GetMaxHealth()
    {
        if(GetComponentInChildren<Slider>() == null) 
        {
            return GameObject.Find("Full Health Bar").GetComponentInChildren<Slider>().maxValue;
        }
        return GetComponentInChildren<Slider>().maxValue ;
    }
}
