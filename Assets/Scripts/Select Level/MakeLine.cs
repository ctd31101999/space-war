﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MakeLine : MonoBehaviour
{
    [SerializeField] private List<Transform> checkPoints = new List<Transform>();
    // Start is called before the first frame update
    private LineRenderer lineRenderer;
    public GameObject controller;
    [SerializeField] GameObject line;
    [SerializeField] GameObject footerLine;
    void Start()
    {
        controller = GameObject.Find("Game Controller");
        controller.GetComponent<MainUIController>().enabled = true;
        foreach(Transform x in checkPoints)
        {
            x.GetComponent<Button>().onClick.AddListener(delegate{LevelButton(x.name);});
        }
        GameObject.Find("Play").GetComponent<Button>().onClick.AddListener( 
            delegate {  
                        GameObject.Find("Game Controller").GetComponent<MainUIController>().enabled = false;
                        GameObject.Find("Game Controller").GetComponent<GameManager>().enabled = true;
                        SceneManager.LoadScene("main");
                        controller.SetActive(true);
                        controller.GetComponent<GameManager>().level = GameObject.Find("Game Controller").GetComponent<MainUIController>().selectLevel; } );
    }

    void LevelButton(string name)
    {
        string [] token = name.Split(' ');
        GameObject.Find("Game Controller").GetComponent<MainUIController>().selectLevel = int.Parse(token[1]);
    }
}
