﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeUI : MonoBehaviour
{
    [SerializeField] GameObject levelUI;
    [SerializeField] GameObject storageUI;
    [SerializeField] GameObject shopUI;
    [SerializeField] Transform weaponContain;
    [SerializeField] Button planetBt;
    [SerializeField] Button shopBt;
    [SerializeField] Button storeBt;
    [SerializeField] Sprite notSelectButton;



    int SetUIButtonColor = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (SetUIButtonColor)
        {
            case 1:
                planetBt.image.sprite = planetBt.spriteState.selectedSprite;
                shopBt.image.sprite = notSelectButton;
                storeBt.image.sprite = notSelectButton;
                break;
            case 2:
                planetBt.image.sprite = notSelectButton;
                shopBt.image.sprite = planetBt.spriteState.selectedSprite;
                storeBt.image.sprite = notSelectButton;                
                break;
            case 3:
                planetBt.image.sprite = notSelectButton;
                shopBt.image.sprite = notSelectButton;
                storeBt.image.sprite = planetBt.spriteState.selectedSprite;                
                break;
        }
    }

    public void ToLevel()
    {
        GameObject.Find("Game Controller").GetComponent<MainUIController>().enabled = true;
        SetUIButtonColor = 1;
        shopUI.SetActive(false);
        storageUI.SetActive(false);
        levelUI.SetActive(true);
    }
    public void ToShop()
    {
        GameObject.Find("Game Controller").GetComponent<MainUIController>().enabled = false;
        SetUIButtonColor = 2;
        storageUI.SetActive(false);
        levelUI.SetActive(false);
        shopUI.SetActive(true);
    }
    public void ToStorage()
    {
        GameObject.Find("Game Controller").GetComponent<MainUIController>().enabled = false;
        SetUIButtonColor = 3;
        shopUI.SetActive(false);
        levelUI.SetActive(false);
        storageUI.SetActive(true);
        if(InventoryUI.storageWeapon != null)
        {
            InventoryUI.storageWeapon();
        }
    }
}
