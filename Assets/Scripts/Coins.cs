﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Coins : HitObject
{
    void Start()
    {
    }
    void Update()
    {
        Move();
        if(!IsOnScreen())
        {
            gameObject.Kill();
        }
    }

    void OnTriggerStay(Collider other)
    {
        if(other.tag=="Player")
        {
            string temp = GameObject.Find("Coins Text").GetComponent<Text>().text;
            temp = (int.Parse(temp)+value).ToString();
            GameObject.Find("Coins Text").GetComponent<Text>().text = string.Join(" ",temp);
            GameObject.Find("Game Controller").GetComponent<CoinAndWeaponManage>().Coins += (int)value;
            gameObject.Kill();
        }
    }
}
