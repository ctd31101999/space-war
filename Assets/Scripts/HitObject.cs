﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HitObject : MonoBehaviour
{
    public float value;
    public string tagObject;
    public float speed;
    public virtual void Move()
    {
        if (speed != 0)
        {
            if( tagObject.Equals("Enemy") )
            {
                transform.position -= transform.forward * (speed * Time.deltaTime);    
            }
            else if( tagObject.Equals("Player") )
            {
                transform.position += transform.forward * (speed * Time.deltaTime);
            }
            else if( tagObject.Equals("Coin") )
            {
                transform.Translate(Vector3.forward*Time.deltaTime);
                transform.Rotate(0.0f, 0.0f, 90.0f*Time.deltaTime);
            } 
        }
    }
    protected bool IsOnScreen()
    {
        return  transform.position.x <= 6 &&
                transform.position.x >= -6 && 
                transform.position.y <= 11 && 
                transform.position.y >= -11;
    }
}
