﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowGun : Weapon
{
    float counterForShoot;
    public int numberBullet;
    bool shooting;
    int count;
    float inShootingTime = 1f;
    Vector3 direction;
    void Start()
    {
        count = 0;
        inShootingTime = 0.2f;
        shooting = true;
        counterForShoot = 1/fireRate;
    }

    void Update()
    {
        Shoot();
    }
    public override void Shoot()
    {
                
        if(shooting)
        {
            //Debug.Log("shooting");
            if(count >= numberBullet) shooting = false;
            inShootingTime -= Time.deltaTime;
            if(inShootingTime <= 0f)
            {
                inShootingTime = 0.2f;
                count++;
                
                GameObject bullet = bulletPrefab.Spawn(transform.position, transform.rotation, new Vector3(1.5f, 1.5f, 0.5f));
                if(count == 1) 
                {
                    direction = bullet.GetComponent<FollowBullet>().setDirection(GameObject.Find("Player"));
                    bullet.GetComponent<FollowBullet>().direction = direction;
                }
                else
                {
                    bullet.GetComponent<FollowBullet>().direction = direction;
                }
                
                // Debug.Log( bullet.GetComponent<FollowBullet>().direction);
                bullet.GetComponent<FollowBullet>().tagObject = transform.parent.tag;
                bullet.GetComponent<FollowBullet>().value = damage;
                // bullet.GetComponent<FollowBullet>().speed = bulletSpeed;
             
            }
        }else
        {
            //Debug.Log("not shooting");
            counterForShoot -= Time.deltaTime;
            if(counterForShoot <= 0f)
            {
                shooting = true;
                counterForShoot = 1/fireRate;
                count = 0;
            }
        }     
    } 
}
