﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBullet : HitObject
{
    public Vector3 direction;
    public float hitOffset = 0f;
    public bool UseFirePointRotation;
    public GameObject hit;
    public GameObject flash;

    void Start()
    {
        direction = setDirection(GameObject.Find("Player"));
        //Debug.Log(direction);
        if (flash != null)
        {
            var flashInstance = Instantiate(flash, transform.position, Quaternion.identity);
            flashInstance.transform.forward = gameObject.transform.forward;
            var flashPs = flashInstance.GetComponent<ParticleSystem>();
            if (flashPs == null)
            {
                Destroy(flashInstance, flashPs.main.duration);
            }
            else
            {
                var flashPsParts = flashInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(flashInstance, flashPsParts.main.duration);
            }
        }
    }
    void Update()
    {
        Move();
        if(!IsOnScreen())
        {
            gameObject.Kill();
        }
    }

    public Vector3 setDirection(GameObject target)
    {
        return target.transform.position - gameObject.transform.position;
    }

    public override void Move()
    {
        if (speed != 0)
        {
            transform.position += direction.normalized * (10f * Time.deltaTime);
            transform.rotation = Quaternion.LookRotation(direction);      
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if( tagObject.Equals("Player") )
        {
            if(other.tag.Equals("Enemy"))
            {
                other.GetComponentInChildren<Enemy>().ReceiveDamage(value);
                this.gameObject.Kill();
            }
        }
        else if( tagObject.Equals("Enemy") )
        {
            if(other.tag.Equals("Player"))
            {
                other.GetComponentInChildren<Player>().ReceiveDamage(value);
                this.gameObject.Kill();
            }
        } 
    }

    void OnCollisionEnter(Collision collision)
    {
        speed = 0;   
        ContactPoint contact = collision.contacts[0];
        Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
        Vector3 pos = contact.point + contact.normal * hitOffset;

        if (hit != null)
        {
            var hitInstance = Instantiate(hit, pos, rot);
            if (UseFirePointRotation)
            { hitInstance.transform.rotation = gameObject.transform.rotation * Quaternion.Euler(0, 180f, 0); }
            else
            { hitInstance.transform.LookAt(contact.point + contact.normal); }

            var hitPs = hitInstance.GetComponent<ParticleSystem>();
            if (hitPs == null)
            {
                Destroy(hitInstance, hitPs.main.duration);
            }
            else
            {
                var hitPsParts = hitInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(hitInstance, hitPsParts.main.duration);
            }
        }
        //Destroy(gameObject);
    }
}
