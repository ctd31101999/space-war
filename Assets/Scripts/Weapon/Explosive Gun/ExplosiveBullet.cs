﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBullet : HitObject
{
    public GameObject miniExplosiveBulletPrefab;
    public float explosiveDistance;
    float timeExplose = 1.0f;
    void Update()
    {
        Move();
        Bump();
        if(!IsOnScreen())
        {
            gameObject.Kill();
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if( tagObject.Equals("Player") )
        {
            if(other.tag.Equals("Enemy"))
            {
                other.GetComponentInChildren<Enemy>().ReceiveDamage(value);
                this.gameObject.Kill();
            }
        }
        else if( tagObject.Equals("Enemy") )
        {
            if(other.tag.Equals("Player"))
            {
                other.GetComponentInChildren<Player>().ReceiveDamage(value);
                this.gameObject.Kill();
            }
        }
    }

    private void Bump()
    {
        float rotate = 0;
        timeExplose -= Time.deltaTime;
        if (timeExplose < 0.0f)
        {
            for (int i = 0; i < 8; i++)
            {
                GameObject miniBullet = miniExplosiveBulletPrefab.Spawn(transform.position, Quaternion.Euler(0.0f, 0.0f, rotate), new Vector3(.4f,.4f,.4f));
                miniBullet.GetComponent<MiniBullet>().tagObject = "Enemy";
                miniBullet.GetComponent<MiniBullet>().value = 4;
                miniBullet.GetComponent<MiniBullet>().speed = 4;
                rotate += 360/8;
            }
            gameObject.Kill();
            timeExplose = 1.0f;
        }
    }
}