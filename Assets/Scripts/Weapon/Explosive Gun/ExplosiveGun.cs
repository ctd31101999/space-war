﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveGun : Weapon
{
    float counterForShoot;
    public int numberBullet;

    void Start()
    {
        counterForShoot = 1/fireRate;
    }
    
    void Update()
    {
        Shoot();
    }

    public override void Shoot()
    {
        counterForShoot-=Time.deltaTime;
        if(counterForShoot <= 0f)
        {
            for(int i = -(numberBullet - 1 )/2; i <= (numberBullet - 1 )/2; i++ )
            {
                GameObject bullet = bulletPrefab.Spawn(transform.position, transform.rotation, new Vector3(0.9f, 0.9f, 0.9f));
                bullet.GetComponent<ExplosiveBullet>().tagObject = transform.parent.tag;
                bullet.GetComponent<ExplosiveBullet>().value = damage;
                // bullet.GetComponent<ExplosiveBullet>().speed = bulletSpeed;
            }   
            counterForShoot = 1/fireRate;
        }
    }
}
