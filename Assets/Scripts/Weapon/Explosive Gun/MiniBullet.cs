﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniBullet : HitObject
{
    void Update()
    {
        transform.position += transform.up * (speed * Time.deltaTime);
        if(!IsOnScreen())
        {
            gameObject.Kill();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if( tagObject.Equals("Player"))
        {
            if(other.tag.Equals("Enemy"))
            {
                other.GetComponentInChildren<Enemy>().ReceiveDamage(value);
                this.gameObject.Kill();
            }
        }
        else if( tagObject.Equals("Enemy") )
        {
            if(other.tag.Equals("Player"))
            {
                other.GetComponentInChildren<Player>().ReceiveDamage(value);
                this.gameObject.Kill();
            }
        }
    }
}
