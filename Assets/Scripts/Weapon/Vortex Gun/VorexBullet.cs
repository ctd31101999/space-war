﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VorexBullet : HitObject
{
    public Vector3 center;
    public float hitOffset = 0f;
    public bool UseFirePointRotation;
    public GameObject hit;
    public GameObject flash;
    // Start is called before the first frame update
    void Start()
    {
        if (flash != null)
        {
            var flashInstance = Instantiate(flash, transform.position, Quaternion.identity);
            flashInstance.transform.forward = gameObject.transform.forward;
            var flashPs = flashInstance.GetComponent<ParticleSystem>();
            if (flashPs == null)
            {
                Destroy(flashInstance, flashPs.main.duration);
            }
            else
            {
                var flashPsParts = flashInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(flashInstance, flashPsParts.main.duration);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        if(!IsOnScreen())
        {
            gameObject.Kill();
        }
        
    }
    
    // void FixedUpdate ()
    // {
	// 	if (speed != 0)
    //     {
    //         Vector3 radialVector = transform.position - center;
    //         Vector3 tangentVector = new Vector3(radialVector.y, -radialVector.x, 0.0f);
    //         Vector3 moveVector = radialVector*0.2f + tangentVector*0.15f;
    //         transform.position += moveVector * (speed * Time.deltaTime);         
    //     }
	// }

    public override void Move()
    {
        if (speed != 0)
        {
            Vector3 radialVector = transform.position - center;
            Vector3 tangentVector = new Vector3(radialVector.y, -radialVector.x, 0.0f);
            Vector3 moveVector = radialVector*0.2f + tangentVector*0.15f;
            transform.position += moveVector * (speed * Time.deltaTime);         
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if( tagObject.Equals("Player") )
        {
            if(other.tag.Equals("Enemy"))
            {
                other.GetComponentInChildren<Enemy>().ReceiveDamage(value);
                this.gameObject.Kill();
            }
        }
        else if( tagObject.Equals("Enemy") )
        {
            if(other.tag.Equals("Player"))
            {
                other.GetComponentInChildren<Player>().ReceiveDamage(value);
                this.gameObject.Kill();
            }
        } 
    }

    void OnCollisionEnter(Collision collision)
    {
        speed = 0;   
        ContactPoint contact = collision.contacts[0];
        Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
        Vector3 pos = contact.point + contact.normal * hitOffset;

        if (hit != null)
        {
            var hitInstance = Instantiate(hit, pos, rot);
            if (UseFirePointRotation)
            { hitInstance.transform.rotation = gameObject.transform.rotation * Quaternion.Euler(0, 180f, 0); }
            else
            { hitInstance.transform.LookAt(contact.point + contact.normal); }

            var hitPs = hitInstance.GetComponent<ParticleSystem>();
            if (hitPs == null)
            {
                Destroy(hitInstance, hitPs.main.duration);
            }
            else
            {
                var hitPsParts = hitInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(hitInstance, hitPsParts.main.duration);
            }
        }
        //Destroy(gameObject);
    }
}
