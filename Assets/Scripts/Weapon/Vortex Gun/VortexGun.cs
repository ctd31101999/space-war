﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class VortexGun : Weapon
{
    float counterForShoot;
    bool shooting;
    int count;
    public int numberBullet;
    float inShootingTime = 1f;
    // Start is called before the first frame update
    Vector3 [] listPosition = { new Vector3(0.5f, 0.0f, 0.0f),
                                new Vector3(-0.5f, 0.0f, 0.0f),
                                new Vector3(0.25f, Mathf.Sqrt(0.5f*0.5f - 0.25f*0.25f) , 0.0f),
                                new Vector3(0.25f, -Mathf.Sqrt(0.5f*0.5f - 0.25f*0.25f), 0.0f),
                                new Vector3(-0.25f, Mathf.Sqrt(0.5f*0.5f - 0.25f*0.25f), 0.0f),
                                new Vector3(-0.25f, -Mathf.Sqrt(0.5f*0.5f - 0.25f*0.25f), 0.0f),};
    void Start()
    {
        inShootingTime = 0.2f;
        shooting = true;
        counterForShoot = 1/fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        Shoot();
    }

    public override void Shoot()
    {
        if(shooting)
        {
            if(count >= numberBullet) shooting = false;
            inShootingTime -= Time.deltaTime;
            if(inShootingTime <= 0f)
            {
                inShootingTime = 0.2f;
                count++;
                for(int i = 0; i < 6; i++ )
                {
                    GameObject bullet = bulletPrefab.Spawn(transform.position + listPosition[i], transform.rotation, Vector3.one);
                    bullet.GetComponent<VorexBullet>().tagObject = transform.parent.tag;
                    bullet.GetComponent<VorexBullet>().center = transform.position;
                    bullet.GetComponent<VorexBullet>().value = damage;
                    // bullet.GetComponent<VorexBullet>().speed = bulletSpeed;
                }   
                counterForShoot = 1/fireRate;
            }
        }
        else
        {
            counterForShoot -= Time.deltaTime;
            if(counterForShoot <= 0f)
            {
                shooting = true;
                counterForShoot = 1/fireRate;
                count = 0;
            }
        }
    }
}
