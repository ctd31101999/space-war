﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalGun : Weapon
{
    float counterForShoot;
    public int numberBullet;
    // Start is called before the first frame update
    void Start()
    {
        counterForShoot = 1/fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        Shoot();
    }

    public override void Shoot()
    {
        counterForShoot-=Time.deltaTime;
        if(counterForShoot <= 0f)
        {   
            float angle =0.0f;
            Vector3 scale = Vector3.one;
            if(transform.parent.tag.Equals("Enemy"))
            {
                Vector3 forward = GameObject.Find("Player").transform.position - transform.position;
                angle = (forward.x>=0?1:-1)*Vector3.Angle(Vector3.down, forward);
                scale *= 1.2f;
            }
            for(int i = -(numberBullet - 1 )/2; i <= (numberBullet - 1 )/2; i++ )
            {
                //GameObject bullet = bulletPrefab.Spawn(transform.position, Quaternion.identity, new Vector3(10f,10f,10f));
                GameObject bullet =  bulletPrefab.Spawn(transform.position, Quaternion.Euler(10f*i - 90f - angle, 90f, 0f), scale);
                bullet.GetComponent<NormalBullet>().tagObject = transform.parent.tag;
                bullet.GetComponent<NormalBullet>().value = damage;
                //bullet.GetComponent<NormalBullet>().speed = bulletSpeed;
            }   
            counterForShoot = 1/fireRate;
        }
    }
}
