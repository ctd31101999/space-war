﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinBullet : HitObject
{
    public int left;
    public Vector3 firePoint;
    Vector3 forward;
    public float hitOffset = 0f;
    public bool UseFirePointRotation;
    public GameObject hit;
    public GameObject flash;
    // Start is called before the first frame update
    void Start()
    {
        forward = GameObject.Find("Player").transform.position - transform.position;
        if (flash != null)
        {
            var flashInstance = Instantiate(flash, transform.position, Quaternion.identity);
            flashInstance.transform.forward = gameObject.transform.forward;
            var flashPs = flashInstance.GetComponent<ParticleSystem>();
            if (flashPs == null)
            {
                Destroy(flashInstance, flashPs.main.duration);
            }
            else
            {
                var flashPsParts = flashInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(flashInstance, flashPsParts.main.duration);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        if(!IsOnScreen())
        {
            gameObject.Kill();
        }
    }

    public override void Move()
    {
        if (speed != 0)
        {
            float newY = transform.position.y - speed*Time.deltaTime;
            float newX = (left==0?1:-1)*0.5f*Mathf.Sin(newY-firePoint.y) + firePoint.x;
            firePoint.x += speed*Time.deltaTime*forward.normalized.x;
            Vector3 destination = new Vector3(newX, newY, transform.position.z);
            transform.position = destination;         
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if( tagObject.Equals("Player") )
        {
            if(other.tag.Equals("Enemy"))
            {
                other.GetComponentInChildren<Enemy>().ReceiveDamage(value);
                this.gameObject.Kill();
            }
        }
        else if( tagObject.Equals("Enemy") )
        {
            if(other.tag.Equals("Player"))
            {
                other.GetComponentInChildren<Player>().ReceiveDamage(value);
                this.gameObject.Kill();
            }
        } 
    }
    void OnCollisionEnter(Collision collision)
    {
        speed = 0;   
        ContactPoint contact = collision.contacts[0];
        Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
        Vector3 pos = contact.point + contact.normal * hitOffset;

        if (hit != null)
        {
            var hitInstance = Instantiate(hit, pos, rot);
            if (UseFirePointRotation)
            { hitInstance.transform.rotation = gameObject.transform.rotation * Quaternion.Euler(0, 180f, 0); }
            else
            { hitInstance.transform.LookAt(contact.point + contact.normal); }

            var hitPs = hitInstance.GetComponent<ParticleSystem>();
            if (hitPs == null)
            {
                Destroy(hitInstance, hitPs.main.duration);
            }
            else
            {
                var hitPsParts = hitInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(hitInstance, hitPsParts.main.duration);
            }
        }
        //Destroy(gameObject);
    }
}
