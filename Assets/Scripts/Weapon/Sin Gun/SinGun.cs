﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinGun : Weapon
{
    float counterForShoot;
    bool shooting;
    int count;
    public int numberBullet;
    float inShootingTime = 1f;
    // Start is called before the first frame update
    void Start()
    {
        inShootingTime = 0.2f;
        shooting = true;
        counterForShoot = 1/fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        Shoot();
    }

    public override void Shoot()
    {
        if(shooting)
        {
            if(count >= numberBullet) shooting = false;
            inShootingTime -= Time.deltaTime;
            if(inShootingTime <= 0f)
            {
                inShootingTime = 0.1f;
                count++;
                for(int i = 0; i < 2; i++ )
                {
                    GameObject bullet = bulletPrefab.Spawn(transform.position, transform.rotation, Vector3.one);
                    bullet.GetComponent<SinBullet>().firePoint = transform.position;
                    bullet.GetComponent<SinBullet>().left = i;
                    bullet.GetComponent<SinBullet>().tagObject = transform.parent.tag;
                    bullet.GetComponent<SinBullet>().value = damage;
                    // bullet.GetComponent<SinBullet>().speed = bulletSpeed;
                }   
                counterForShoot = 1/fireRate;
            }
        }
        else
        {
            counterForShoot -= Time.deltaTime;
            if(counterForShoot <= 0f)
            {
                shooting = true;
                counterForShoot = 1/fireRate;
                count = 0;
            }
        }
    }
}
