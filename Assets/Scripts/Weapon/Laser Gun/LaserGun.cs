﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserGun : Weapon
{
    GameObject Instance;
    private EGA_Laser LaserScript;
    public float Width;
    public bool oneTime = false;
    public bool wait = true;
    // Start is called before the first frame update
    void Start()
    {
        Instance = Instantiate(bulletPrefab, transform.position, transform.rotation);
        Instance.transform.localScale = new Vector3(Width, Width, Width);
        Instance.GetComponent<EGA_Laser>().value = damage;
        Instance.GetComponent<EGA_Laser>().width = Width/4;
        Instance.GetComponent<EGA_Laser>().tagObject = transform.parent.tag;
        Instance.transform.parent = transform;
    }

    // Update is called once per frame
    void Update()
    {
        Shoot();
    }
    public override void Shoot()
    {
        if(oneTime)
        {
            Destroy(Instance);
            Instance = Instantiate(bulletPrefab, transform.position, transform.rotation);
            Instance.transform.localScale = new Vector3(Width, Width, Width);
            Instance.GetComponent<EGA_Laser>().value = damage;
            Instance.GetComponent<EGA_Laser>().width = Width/4;
            Instance.GetComponent<EGA_Laser>().tagObject = transform.parent.tag;
            Instance.transform.parent = transform;
            if(wait)
            {
                Instance.GetComponent<EGA_Laser>().enabled = false;
                Instance.transform.Find("Flash").gameObject.SetActive(false);
                Instance.transform.Find("SemiDark").gameObject.SetActive(false);
            }
            oneTime = false;
        }
    }
}
