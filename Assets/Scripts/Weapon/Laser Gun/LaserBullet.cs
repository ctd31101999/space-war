﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class LaserBullet : HitObject
{
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Coin") || other.name.Contains("Bullet") || other.name.Contains("Explosion") ) return;
        if( tagObject.Equals("Player") )
        {
            if(other.tag.Equals("Enemy"))
            {
                other.GetComponentInChildren<Enemy>().ReceiveDamage(value);
            }
        }
        else if( tagObject.Equals("Enemy") )
        {
            if( other.tag.Equals("Player") )
            {
                other.GetComponentInChildren<Player>().ReceiveDamage(value);
            }
        }
    }
}
