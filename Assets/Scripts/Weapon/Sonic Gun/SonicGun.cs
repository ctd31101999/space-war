﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class SonicGun : Weapon
{
    float counterForShoot;
    public int numberBullet;
    // Start is called before the first frame update
    void Start()
    {
        counterForShoot = 1/fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        Shoot();
    }

    public override void Shoot()
    {
        counterForShoot-=Time.deltaTime;
        if(counterForShoot <= 0f)
        {
            for(int i = -(numberBullet - 1 )/2; i <= (numberBullet - 1 )/2; i++ )
            {
                GameObject bullet = bulletPrefab.Spawn(transform.position + new Vector3(0.3f*i, -0.3f*Mathf.Abs(i), 0.0f), transform.rotation, Vector3.one);
                bullet.GetComponent<SonicBullet>().tagObject = transform.parent.tag;
                bullet.GetComponent<SonicBullet>().value = damage;
                // bullet.GetComponent<SonicBullet>().speed = bulletSpeed;
            }   
            counterForShoot = 1/fireRate;
        }
    }
}
