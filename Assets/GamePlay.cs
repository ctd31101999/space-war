﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GamePlay : MonoBehaviour
{
    [SerializeField] GameObject controller;
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(controller);
        SceneManager.LoadScene("Select Level");
        controller.SetActive(true);
    }

    void finish()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
    }
}
